import 'package:flutter/material.dart';
import 'package:quatrace/models/user.dart';
import 'package:quatrace/utils/api-util.dart';
import 'package:quatrace/utils/widget-utils.dart';

class Statistics extends StatefulWidget {
  Statistics({Key key}) : super(key: key);

  @override
  _StatisticsState createState() => _StatisticsState();
}

class _StatisticsState extends State<Statistics> {
  User _currentUser;
  bool _isLoading = true;
  @override
  void initState() {
    _fetchUser();
    super.initState();
  }

  Future<void> _fetchUser() async {
    setState(() {
      _isLoading = true;
    });
    User result = await APIUtil().getUserDetails();
    setState(() {
      _currentUser = result;
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading == true
        ? WidgetUtils().generateLoader(context, "Fetching your data...")
        : RefreshIndicator(
            child: Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text(
                  "Validations",
                  style: Theme.of(context).textTheme.title,
                ),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: _showQuarantines(context, _currentUser),
                  )
                ],
              ),
            ),
            onRefresh: _fetchUser,
          );
  }
}

showInfoDialog(information, context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        titlePadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        title: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0)),
          ),
          padding: EdgeInsets.all(20.0),
          child: Text(
            "Validation infromation",
            style: TextStyle(color: Colors.white),
          ),
        ),
        content: Container(
          width: 200.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Status: ",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
                  ),
                  Text(information.status,
                      style: TextStyle(
                          color: colorBasedOnStatus(information.status)))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Verified at: ",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
                  ),
                  Text(information.updatedAt)
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Difference: ",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
                  ),
                  Text(information.distance)
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Image equality: ",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
                  ),
                  Text(information.imageEquality)
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Image verification: ",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
                  ),
                  information.imageVerificationIcon
                ],
              ),
              SizedBox(height: 15.0),
              Text(
                "Reason",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
              ),
              Text(
                information.reason,
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
              )
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            color: Theme.of(context).accentColor,
            child: Text(
              "Ok",
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
          )
        ],
      );
    },
  );
}

Color colorBasedOnStatus(String status) {
  if (status == 'Approved') {
    return Colors.green;
  }
  if (status == 'Expired') {
    return Colors.red;
  }
  return Colors.blueAccent;
}

_showQuarantines(BuildContext context, User currentUser) {
  if (currentUser.quarantineEntries.length == 0) {
    return Container(
      color: Colors.grey.shade200,
      child: Center(
        child: Text(
          "No entries yet",
          style: Theme.of(context).textTheme.display1,
        ),
      ),
    );
  }
  final List entries = currentUser.quarantineEntries;
  return Container(
    color: Colors.grey.shade200,
    child: ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      padding: EdgeInsets.all(5.0),
      itemCount: entries.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: ListTile(
            title: Text(
              entries[index].status,
              style: TextStyle(
                  color: colorBasedOnStatus(entries[index].status),
                  fontSize: 18.0),
            ),
            subtitle: Row(
              children: <Widget>[
                Text("Expires at "),
                Text(entries[index].expiresAt)
              ],
            ),
            trailing: IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  showInfoDialog(entries[index], context);
                }),
          ),
        );
      },
    ),
  );
}
