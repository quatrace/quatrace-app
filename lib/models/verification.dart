import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

class Verification {
  String _status;
  String _updatedAt;
  String _distance;
  String _imageEquality;
  Icon _imageVerificationIcon;
  String _expiresAt;
  String _reason;

  get status => this._status;
  get updatedAt => this._updatedAt;
  get distance => this._distance;
  get imageEquality => this._imageEquality;
  get imageVerificationIcon => this._imageVerificationIcon;
  get expiresAt => this._expiresAt;
  get reason => this._reason;

  Verification.mapFields(Map<String, dynamic> verifications) {
    this._reason = verifications['reason'] ??= 'N/A';
    this.parseStatus(verifications['status']);
    this._updatedAt = this.parseDate(verifications['updated_at']);
    this._distance = this.calculateDifference(verifications['distance']);
    this._imageEquality = "${verifications['confidence'] ??= 0}%";
    this._imageVerificationIcon = this.setVerificationICon(verifications['is_identical']);
    this._expiresAt = this.parseDate(verifications['expires_at']);
  }

  void parseStatus(String status) {
    if (status == 'REJECTED' && this._reason == 'Expired') {
      this._status = 'Expired';
      return;
    }
    String lowerCaseStatus = status.toLowerCase();
    this._status =
        lowerCaseStatus[0].toUpperCase() + lowerCaseStatus.substring(1);
  }

  String parseDate(date) {
    if (date == null) {
      return '';
    }
    DateTime _convertedDate = DateTime.fromMillisecondsSinceEpoch(date * 1000);
    return DateFormat('dd-MMM-yyyy HH:mm').format(_convertedDate).toString();
  }

  String calculateDifference(uncalculatedDistance) {
    if (uncalculatedDistance == null) {
      return '0m';
    }
    return "${uncalculatedDistance.toStringAsFixed(0)}m";
  }

  Icon setVerificationICon(isIdentical) {
    if (isIdentical == null || isIdentical == false || isIdentical == 0) {
      return Icon(
        Icons.clear,
        color: Colors.red,
        size: 28.0,
      );
    }
    return Icon(
      Icons.done,
      color: Colors.green,
      size: 28.0,
    );
  }
}
